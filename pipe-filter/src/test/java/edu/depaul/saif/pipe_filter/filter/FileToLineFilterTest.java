package edu.depaul.saif.pipe_filter.filter;

import edu.depaul.saif.pipe_filter.fitler.FileParserException;
import edu.depaul.saif.pipe_filter.fitler.FileToLineFilter;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileToLineFilterTest {

    private FileToLineFilter fileToLineFilter;

    @Before
    public void setup() {
        fileToLineFilter = new FileToLineFilter();
    }

    @Test
    public void verify_that_no_of_lines_are_parsed_correctly() {
        List<String> lines = fileToLineFilter.parse("news_test.txt");
        assertEquals(4, lines.size());
    }

    @Test(expected = NullPointerException.class)
    public void verify_exception_thrown_when_filename_is_null() {
        fileToLineFilter.parse(null);
    }

    @Test(expected = FileParserException.class)
    public void verify_exception_thrown_when_filename_is_empty() {
        fileToLineFilter.parse("");
    }
}
