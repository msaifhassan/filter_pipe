package edu.depaul.saif.pipe_filter.filter;

import edu.depaul.saif.pipe_filter.fitler.GroupingFilter;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GroupingFilterTest {
    private static final String BOB = "bob";
    private static final String UNIQUE = "unique";
    private static final String GOOGLE = "google";
    private static final String ALICE = "alice";
    private static List<String> WORDS = Arrays.asList(BOB, BOB, UNIQUE, ALICE, GOOGLE, BOB, ALICE, ALICE, GOOGLE);
    private GroupingFilter groupingFilter;

    @Before
    public void setup() {

    }

    @Test(expected = NullPointerException.class)
    public void test_exception_thrown_when_words_is_null() {
        groupingFilter.group(null);
    }

    @Test
    public void test_that_grouping_and_counting_correct() {
        groupingFilter = new GroupingFilter(3);
        Map<String, Long> grouping = groupingFilter.group(WORDS);

        assertEquals(grouping.size(), 3);
        assertEquals(grouping.get(ALICE).longValue(), 3);
        assertEquals(grouping.get(BOB).longValue(), 3);
        assertEquals(grouping.get(GOOGLE).longValue(), 2);
    }
}
