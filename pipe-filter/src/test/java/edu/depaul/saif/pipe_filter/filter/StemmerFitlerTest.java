package edu.depaul.saif.pipe_filter.filter;

import edu.depaul.saif.pipe_filter.fitler.StemmerFilter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StemmerFitlerTest {


    @Test
    public void test_that_stem_words_are_converted_correctly() {
        String word1 = "jumping";
        String word2 = "jumps";
        String word3 = "jumped";
        assertEquals("jump", new StemmerFilter(word1).stemAndToString());
        assertEquals("jump", new StemmerFilter(word2).stemAndToString());
        assertEquals("jump", new StemmerFilter(word3).stemAndToString());
    }

    @Test(expected = NullPointerException.class)
    public void test_exception_thrown_when_word_is_null() {
        new StemmerFilter(null);
    }
}
