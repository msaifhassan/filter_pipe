package edu.depaul.saif.pipe_filter.filter;

import edu.depaul.saif.pipe_filter.fitler.LowercaseFilter;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LowercaseFilterTest {
    private final static List<String> WORDS = Arrays.asList("TrumPed", "Today", "Bye!.Bye");
    private LowercaseFilter lowerCaseFilter;

    @Before
    public void setup() {
        lowerCaseFilter = new LowercaseFilter();
    }

    @Test(expected = NullPointerException.class)
    public void test_exception_thrown_when_words_list_is_null() {
        lowerCaseFilter.toLowerCase(null);
    }

    @Test
    public void test_words_are_in_lower_case() {
        List<String> expectedWords = lowerCaseFilter.toLowerCase(WORDS);

        assertEquals(expectedWords.size(), WORDS.size());
        assertTrue(expectedWords.contains("trumped"));
        assertTrue(expectedWords.contains("today"));
        assertTrue(expectedWords.contains("bye!.bye"));
    }
}
