package edu.depaul.saif.pipe_filter.filter;

import edu.depaul.saif.pipe_filter.fitler.NonAlphabetFilter;
import edu.depaul.saif.pipe_filter.fitler.NonAlphabetParserException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NonAlphabetFilterTest {
    private final static String WORDS_1 ="Tom's.";
    private final static String WORDS_2 ="@Jerry!@!";

    private NonAlphabetFilter nonAlphabetFilter;

    @Before
    public void setup() {
        nonAlphabetFilter = new NonAlphabetFilter();
    }

    @Test
    public void verify_that_non_alphabets_chars_are_removed() {
        List<String> actualWords = nonAlphabetFilter.parse(Arrays.asList(WORDS_1, WORDS_2));

        assertEquals("Toms", actualWords.get(0));
        assertEquals("Jerry", actualWords.get(1));
    }

    @Test(expected = NullPointerException.class)
    public void verify_exception_thrown_when_word_is_null() {
        nonAlphabetFilter.parse(null);
    }

    @Test(expected = NonAlphabetParserException.class)
    public void verify_exception_thrown_when_word_is_empty() {
        nonAlphabetFilter.parse(Collections.EMPTY_LIST);
    }
}
