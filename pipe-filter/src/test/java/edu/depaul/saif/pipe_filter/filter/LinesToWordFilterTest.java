package edu.depaul.saif.pipe_filter.filter;

import edu.depaul.saif.pipe_filter.fitler.LineToWordFilterException;
import edu.depaul.saif.pipe_filter.fitler.LinesToWordFilter;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class LinesToWordFilterTest {

    private final static String LINE_1 = "A US appeals court decision Monday against President Donald Trump's effort to keep his tax returns from a grand jury marks the second major court ruling in recent weeks rejecting bold assertions from the President's lawyers that would shield him from investigation.";

    private LinesToWordFilter linesToWordFilter;

    @Before
    public void setup() {
        linesToWordFilter = new LinesToWordFilter();
    }

    @Test(expected = LineToWordFilterException.class)
    public void verify_error_thrown_when_text_is_empty() {
        linesToWordFilter.toWords("");
    }

    @Test(expected = NullPointerException.class)
    public void verify_error_thrown_when_text_is_null() {
        linesToWordFilter.toWords((String) null);
    }

    @Test
    public void verify_no_of_words_are_correct() {
        List<String> words = linesToWordFilter.toWords(LINE_1);
        assertEquals(42, words.size());
    }

    @Test
    public void verify_no_of_words_in_multiple_lines_are_correct() {
        List<String> words = linesToWordFilter.toWords(Arrays.asList(LINE_1, LINE_1));
        assertEquals(84, words.size());
    }
}
