package edu.depaul.saif.pipe_filter.filter;

import edu.depaul.saif.pipe_filter.fitler.FileToLineFilter;
import edu.depaul.saif.pipe_filter.fitler.LinesToWordFilter;
import edu.depaul.saif.pipe_filter.fitler.StopWordsFitler;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StopWordsFilterTest {

    private StopWordsFitler stopWordsFitler;

    @Before
    public void setup() {
        final FileToLineFilter fileToLineFilter = new FileToLineFilter();
        final LinesToWordFilter linesToWordFilter = new LinesToWordFilter();
        final List<String> lines = fileToLineFilter.parse("stopwords.txt");
        final List<String> stopWords = linesToWordFilter.toWords(lines);
        stopWordsFitler = new StopWordsFitler(stopWords);
    }

    @Test(expected = NullPointerException.class)
    public void test_exception_thrown_when_words_is_null() {
        stopWordsFitler.filter(null);
    }

    @Test
    public void test_that_stop_words_are_removed() {
        List<String> words = Arrays.asList("the", "a", "from", "alice", "bob", "although");

        List<String> filteredWords = stopWordsFitler.filter(words);
        List<String> expectedFilteredWords = Arrays.asList("alice", "bob");
        assertEquals(expectedFilteredWords, filteredWords);
    }
}
