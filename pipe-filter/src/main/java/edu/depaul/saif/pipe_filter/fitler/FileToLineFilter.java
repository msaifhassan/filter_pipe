package edu.depaul.saif.pipe_filter.fitler;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileToLineFilter {
    public List<String> parse(String fileName) {
        Objects.requireNonNull(fileName, "filename should not be null");
        if (fileName.isEmpty()) {
            throw new FileParserException("filename should not be empty");
        }
        try {
            System.out.println("Parsing " + fileName + " file");
            Path path = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                    .getResource(fileName)).toURI());

            Stream<String> lines = Files.lines(path);
            return lines.map(String::trim).filter(l -> !l.isEmpty()).collect(Collectors.toList());
        } catch (Exception e) {
            throw new FileParserException("Failed to parse file - " + fileName, e);
        }
    }
}
