package edu.depaul.saif.pipe_filter.fitler;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class NonAlphabetFilter {

    public List<String> parse(List<String> words) {
        Objects.requireNonNull(words, "words should not be null");
        if (words.isEmpty()) {
            throw new NonAlphabetParserException("words should not be empty");
        }
        return words.stream().parallel().map(m -> m.replaceAll("[^a-zA-Z]", "")).filter(m -> !m.isEmpty()).collect(Collectors.toList());
    }
}
