package edu.depaul.saif.pipe_filter.fitler;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class LinesToWordFilter {

    public List<String> toWords(String text) {
        Objects.requireNonNull(text, "Text should not be null");
        if (text.isEmpty()) {
            throw new LineToWordFilterException("Text should not be null");
        }
        return Arrays.asList(text.split("\\s+"));
    }


    public List<String> toWords(List<String> lines) {
        Objects.requireNonNull(lines, "Lines should not be null");
        return lines.stream().parallel().flatMap(l -> this.toWords(l).stream()).map(String::trim).filter(w -> !w.isEmpty()).collect(Collectors.toList());
    }

}
