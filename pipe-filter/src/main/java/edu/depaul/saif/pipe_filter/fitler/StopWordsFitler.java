package edu.depaul.saif.pipe_filter.fitler;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StopWordsFitler {
    private final List<String> stopWords;

    public StopWordsFitler(List<String> stopWords) {
        this.stopWords = stopWords;
    }

    public List<String> filter(List<String> words) {
        Objects.requireNonNull(words, "Words should not be null");
        return words.stream().parallel().filter(w -> !stopWords.contains(w)).collect(Collectors.toList());
    }
}
