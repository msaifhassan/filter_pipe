package edu.depaul.saif.pipe_filter.fitler;

public class LineToWordFilterException extends RuntimeException {


    public LineToWordFilterException(String message) {
        super(message);
    }

    public LineToWordFilterException(String message, Throwable cause) {
        super(message, cause);
    }
}
