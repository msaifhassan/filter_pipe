package edu.depaul.saif.pipe_filter.fitler;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class GroupingFilter {

    private final int limit;

    public GroupingFilter(int limit) {
        this.limit = limit;
    }

    /**
     * Reference https://stackoverflow.com/questions/29122394/word-frequency-count-java-8
     *
     * @param terms
     * @return
     */
    public Map<String, Long> group(List<String> terms) {
        Objects.requireNonNull(terms, "terms should not be null");

        System.out.println("grouping the terms");
        System.out.println("grouping the terms");
        final Map<String, Long> grouping = terms.stream().collect(groupingBy(Function.identity(), counting()));


        // Sorting
        // reference: https://stackoverflow.com/questions/29122394/word-frequency-count-java-8
        return  grouping.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue(Comparator.reverseOrder()).thenComparing(Map.Entry.comparingByKey()))
                .limit(this.limit)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (v1, v2) -> {
                            throw new IllegalStateException();
                        },
                        LinkedHashMap::new
                ));
    }
}
