package edu.depaul.saif.pipe_filter;

import edu.depaul.saif.pipe_filter.fitler.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    private final static int LIMIT = 10;
    private static final StopWordsFitler stopWordsFitler;
    private static final FileToLineFilter fileToLineFilter = new FileToLineFilter();
    private static final LinesToWordFilter linesToWordFilter = new LinesToWordFilter();
    private static final LowercaseFilter lowercaseFilter = new LowercaseFilter();
    private static final NonAlphabetFilter nonAlphabetFilter = new NonAlphabetFilter();
    private static final GroupingFilter groupingFilter = new GroupingFilter(LIMIT);

    private static int COUNT = 0;


    static {
        final List<String> lines = fileToLineFilter.parse("stopwords.txt");
        final List<String> stopWords = linesToWordFilter.toWords(lines);
        stopWordsFitler = new StopWordsFitler(stopWords);
    }

    public static void main(String[] args) {
        System.out.println();
        System.out.println("----------------");
        String declarationFilename = "usdeclar.txt";
        printTopTerms(declarationFilename);
        System.out.println();

        System.out.println("----------------");
        String aliceFilename = "alice30.txt";
        printTopTerms(aliceFilename);
        System.out.println();

        System.out.println("----------------");
        String bibleFilename = "kjbible.txt";
        printTopTerms(bibleFilename);
        System.out.println();

    }

    private static void printTopTerms(String filename) {
        Long startTime = System.currentTimeMillis();
        final List<String> lines = fileToLineFilter.parse(filename);
        final List<String> allWords = linesToWordFilter.toWords(lines);
        final List<String> pureWords = nonAlphabetFilter.parse(allWords);
        final List<String> pureWordsWithLowerCase = lowercaseFilter.toLowerCase(pureWords);
        final List<String> wordsWithoutStopWords = stopWordsFitler.filter(pureWordsWithLowerCase);
        final List<String> terms = wordsWithoutStopWords.stream().parallel().map(w -> new StemmerFilter(w).stemAndToString()).collect(Collectors.toList());
        final Map<String, Long> groupedTerms = groupingFilter.group(terms);



        System.out.println("Printing top " + LIMIT + " terms");
        COUNT = 0;
        for (Map.Entry<String, Long> groupedTerm : groupedTerms.entrySet()) {
            COUNT++;
            System.out.println(COUNT + ") " +groupedTerm.getKey() + " = " + groupedTerm.getValue());
        }
        Long endTime = System.currentTimeMillis();
        Long duration = endTime - startTime;

        System.out.println("Completed processing in " + duration + " millis");
    }
}
