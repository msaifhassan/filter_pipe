package edu.depaul.saif.pipe_filter.fitler;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class LowercaseFilter {
    public List<String> toLowerCase(List<String> words) {
        Objects.requireNonNull(words, "words should not be null");
        return words.stream().parallel().map(String::toLowerCase).collect(Collectors.toList());
    }
}
