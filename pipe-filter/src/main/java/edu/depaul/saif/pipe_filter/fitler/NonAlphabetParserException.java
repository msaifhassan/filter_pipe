package edu.depaul.saif.pipe_filter.fitler;

public class NonAlphabetParserException extends RuntimeException {

    public NonAlphabetParserException(String message) {
        super(message);
    }

    public NonAlphabetParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
