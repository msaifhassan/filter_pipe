package edu.depaul.saif.pipe_filter.fitler;

public class FileParserException extends RuntimeException {


    public FileParserException(String message) {
        super(message);
    }

    public FileParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
